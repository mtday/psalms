# Chapter 1

> 1. Blessed is the man that walketh not in the counsel of the ungodly, nor standeth in the way of sinners, nor sitteth in the seat of the scornful.

God, you are the creator of this world and everything in it, and all our blessings come from you. This verse promises blessings and we thank you for continuously blessing us. Lord we confess those times when we listen to the ungodly people in our lives who pressure us to stray from your guidance, your Word, and your presense, and we fall into the temptations of this world. Lord God, help us ignore the advice of the ungodly and those that lead us into sin in our lives, and instead help us be a witness of your love and grace and mercy. Help us be good examples of Christ to our friends, family, and coworkers every day. We pray You give us this blessing as we strive to avoid the counsel of the ungodly.

> 2 But his delight is in the law of the Lord; and in his law doth he meditate day and night.

God we thank you for giving us your Word, which gives us guidance on your laws and instructions for our lives. Lord God you know that following all of your laws is difficult and we struggle with staying on the path you desire for us. Lord we often fail to meditate on Your Word and we continually skip our duty to read your Word daily. Lord we thank You for giving us the Bible as an instruction manual for our lives. Help us to enjoy doing what is right in Your eyes, and delight in following your commandments. Give us clarity as we read and study your Word. Help us to seek you first and think about you constantly.

> 3 And he shall be like a tree planted by the rivers of water, that bringeth forth his fruit in his season; his leaf also shall not wither; and whatsoever he doeth shall prosper.

God you give us a beautiful picture of your creation in this verse, and remind us that you are creator of all things. Lord God we thank you for the promises in this verse to those who keep your law and mediate on your Word. You give us strength like a firmly rooted tree, able to withstand the waters of life around us that sometimes rage in torrents against us. We confess to sometimes having seasons of fruitlessness, Lord God, help us to continually focus on bearing fruit for you. Help us be a witness for you at all times, and show your spirit to those in our lives. God help us read your Word daily and seek you first in all things, so our leaves do not wither. God, you are all-powerful and guide our lives with your loving hands, and promise prosperity to us as we delight in you and mediate on your Word continually. We thank you for the prosperity of health, family, food, and shelter and we focus on not taking all your blessings for granted.

> 4 The ungodly are not so: but are like the chaff which the wind driveth away.

Lord God keep us on that narrow path that leads to You, and help us remain holy as You are holy. Our only ability to stay Godly comes from you and the grace you have given us. God help us put on your whole armor so that we can withstand the temptations of the devil, and not become ungodly and driven away from you by the winds of this world. God help us be a faithful representation of your loving Spirit to the ungodly all around us. Help us be a witness for you and plant the seeds of Your salvation into the ungodly so you can grow them and change them into Your children.

> 5 Therefore the ungodly shall not stand in the judgment, nor sinners in the congregation of the righteous.

Lord God you are the ultimate judge over all things, and your justice is perfect. We thank you for your Bible and the laws you give us to help keep us on Your holy path. God prepare us for that time in the future when we are called to stand in Your judgement. Cover our sins with the blood of Your son Jesus and what he did for us on the cross. God you tell us not to forsake the assembly of ourselves together, and we confess to sometimes missing services in our local church. God we ask your help in realigning our priorities, help us stay involved in our local congregations and church communities.

> 6 For the Lord knoweth the way of the righteous: but the way of the ungodly shall perish.

Lord God you are all-knowing and master of all things. You lay the paths for our lives and let us freely choose whether we are to follow You in the path of righteousness, or stray off Your path and follow our own ungodly desires. We know Your way is always best for us, yet we confess to often losing sight of Your will for our lives as we follow our own priorities and pursuits. God we thank You for your guiding hand in our lives and we ask that you help us see Your will in our lives. We ask that you help us listen for Your still small voice, and help us hear it even through the shouting of worldly temptations all around us. Give us the strength and will to follow You on Your path, since only You are the way, the truth, and the life.

