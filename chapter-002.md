# Chapter 2

> 1. Why do the heathen rage, and the people imagine a vain thing?

Lord God you point out ungodly anger and imaginations as two problem areas. God we confess to sometimes letting our anger control us, and we confes to often allowing our imaginations tempt us into sins. You are the Prince of Peace and we ask that you give us a spirit of peace when anger begins to boil. God we ask that You help us seek You first and meditate on Your Word continuously to prevent our imaginations from running wild.

> 2. The kings of the earth set themselves, and the rulers take counsel together, against the Lord, and against his anointed, saying,

Lord God you are the perfect King of Kings and Lord of Lords. God we ask that You be with our politicians and leaders and use Your guiding hand to influence their decisions and plans in our country. God our leaders often work against You in their positions of power. Give us the courage to be the voice of dissent, give us the will to vote for You, and give us a heart of compassion for those who disagree. Lord we ask Your blessing on our country even as we stray further and further from Your will.

> 3. Let us break their bands asunder, and cast away their cords from us.

Lord god we praise You for Your influence on this earth. We ask that you change the hearts of our leaders so they recognize and proclaim Your glory. God we ask that You give Your people the strength and courage to hold our ungodly leaders in restraint through the prayers we place on them, and the public pressure of our voices and our votes. 

> 4. He that sitteth in the heavens shall laugh: the Lord shall have them in derision.

God we recognize You as king over everything. You sit in heaven and watch us and direct us, You laugh at those who work against You. Your plans will always be fulfilled on this earth, no matter who stands against You. God we confess to sometimes opposing You and Your will, help us recognize those situations and turn from them. We ask for Your strength in furthering Your kingdom against the opposition around us.

> 5. Then shall he speak unto them in his wrath, and vex them in his sore displeasure.

Lord God You speak out against those who work against You. We ask for courage so that we can also speak out against them. God we confess to often being quiet and letting evil run amok around us unopposed. God this verse says You speak to those who displease You, surely You speak even more to those who seek to please You. God we ask that You speak loudly to us and make Your will obvious since we often do not hear Your still small voice.

> 6. Yet have I set my king upon my holy hill of Zion.

Lord God, You are king of all the earth. You are in control of all things, including those leaders in our country and throughout the world. God You sent Your Son Jesus to us 2,000 years ago, and we look forward to His return at the second coming when Your kingdom is established here. Jesus came to us in humility the first time, and will come again in His full glory and might. God we confess to often fretting over the politics of this world. We ask Your help in surrendering our desire for control in this area to You.

> 7. I will declare the decree: the Lord hath said unto me, Thou art my Son; this day have I begotten thee.

Lord God You are king of kings and lord of lords. You are the creator of this world and everything in it. And God, You are our Father. You bring us into Your family even though we are unworthy. God we look forward to joining You in heaven when our time on this earth ends. Lord God we ask for Your grace and mercy for our friends and family who have not yet become Your children. Help us be the light that leads them toward You.

> 8. Ask of me, and I shall give thee the heathen for thine inheritance, and the uttermost parts of the earth for thy possession.

Lord God You are the source of all blessings and we thank You for everything You have given us. You promise us anything we ask for, and we struggle not to abuse this promise in our greed. God we ask for a spirit of contentment and appreciation for what You have given us. God we confess to sometimes being lured into the materialistic mindset of this world. Give us strength to resist the unnecessary and to be good stewards of what You have provided.

> 9. Thou shalt break them with a rod of iron; thou shalt dash them in pieces like a potter's vessel.

> 10. Be wise now therefore, O ye kings: be instructed, ye judges of the earth.

> 11. Serve the Lord with fear, and rejoice with trembling.

> 12. Kiss the Son, lest he be angry, and ye perish from the way, when his wrath is kindled but a little. Blessed are all they that put their trust in him.

